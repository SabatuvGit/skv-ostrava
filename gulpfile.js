var gulp = require('gulp');
var sass = require('gulp-sass');
var runSequence = require('run-sequence');
var named = require('vinyl-named');
var webpack = require('webpack-stream');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var cssnano = require('gulp-cssnano');
var hologram = require('gulp-hologram');

gulp.task('sass', function () {
    return gulp.src('www/scss/style-*.scss')
            .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest('www/css'));
});

gulp.task('cssmin', function () {
    return gulp.src(['www/css/style-*.css', '!www/css/style-*.min.css'])
            .pipe(cssnano())
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(gulp.dest('www/css'));
});

gulp.task('copy', function () {
    return gulp.src('./www/bower_components/bootstrap-sass/assets/fonts/**/*')
            .pipe(gulp.dest('./www/fonts'));
});

gulp.task('transpile', function () {
    var config = {
        name: 'js',
        module: {
            loaders: [
                {test: /\.es6$/, loader: 'babel', query: {presets: ['es2015']}}
            ]
        }
    };

    return gulp.src(['www/scripts/app-*.es6'])
            .pipe(named())
            .pipe(webpack(config))
            .pipe(gulp.dest('www/js'));
});

gulp.task('jsmin', function () {
    return gulp.src(['www/js/app-*.js', '!www/js/app-*.min.js'])
            .pipe(uglify().on('error', function(error){
                console.log('Message: ' + error.message + ' Line: ' + eror.lineNumber);
            }))
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(gulp.dest('www/js'));
});

gulp.task('hologram', function() {
    return gulp.src('docs/hologram/config.yml')
            .pipe(hologram({
                bundler: true,
                logging: true
            }));
});

gulp.task('css', function () {
    return runSequence('sass', 'cssmin');
});

gulp.task('js', function () {
    return runSequence('transpile', 'jsmin');
});

gulp.task('watch', function () {
    gulp.watch(['./www/scss/**/*.scss'], ['css']);
    gulp.watch(['./www/scripts/**/*.es6'], ['js']);
});

gulp.task('production', function () {
    runSequence(['sass', 'copy']);
});

gulp.task('default', function () {
    return runSequence(['css', 'js', 'copy'], ['watch', 'hologram']);
});

