<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Components\UnusedImagesForm;

use App\Components\BaseControl;
use App\Model\ImagesModel;
use Nette\Application\UI\Form;
use Nette\Utils\Html;

/**
 * Description of UnusedImagesForm
 *
 * @author sabat
 */
class UnusedImagesForm extends BaseControl {

    private $imagesModel = NULL;
    private $relationEntityId;

    public function __construct(ImagesModel $photosModel) {
        $this->imagesModel = $photosModel;
    }

    public function render() {
        $this->template->render(__DIR__ . '/templates/unusedImages.latte');
    }

    public function setRelationEntityId($name) {
        $this->relationEntityId = $name;
    }

    public function createComponentForm() {
        $form = new Form();
        if (!isset($this->relationEntityId)) {
            return new \Nette\InvalidArgumentException('relationEntityId is not set up');
        }
        $photos = $this->imagesModel->getImagesByGallery($this->relationEntityId);
        if (!$photos) {
            return $form;
        } else {
            foreach ($photos AS $photo) {
                $button = $form->addSubmit($photo->id, $photo->name);

                $image = Html::el('img');
                $image->alt = $photo->name;
                $image->src = $photo->linkIcon;

                $button->controlPrototype->setName('button')->add($image);
            }
            // Add external success actions
            foreach ($this->onSuccess AS $action) {
                $form->onSuccess[] = $action;
            }
        }

        return $form;
    }

}

interface IUnusedImagesForm {

    /**
     * @return UnusedImagesForm
     */
    public function create();
}
