<?php

namespace App\Components\RemoveConfirmForm;

use App\Components\BaseControl;
use Nette\Application\UI\Form;
use Nette\InvalidArgumentException;

/**
 * Description of WelcomeImg
 *
 * @author sabat
 */
class RemoveConfirmForm extends BaseControl {

    /**
     * @var int
     */
    private $relationEntityId;

    public function render() {
        $this->template->render(__DIR__ . '/templates/default.latte');
    }

    public function setRelationEntityId($id) {
        $this->relationEntityId = $id;
    }

    public function createComponentConfirmForm() {
        $form = new Form();
        if (!isset($this->relationEntityId)) {
            return new InvalidArgumentException('relationEntityId is not set up');
        }
        $form->addHidden('id',$this->relationEntityId);
        $form->addSubmit('remove', 'Ano');
        $form->addSubmit('cancel', 'Storno')
                ->setAttribute('class','btn btn-success');

        // Add external success actions
        foreach ($this->onSuccess AS $action) {
            $form->onSuccess[] = $action;
        }

        return $form;
    }

}

interface IRemoveConfirmForm {

    /**
     * @return RemoveConfirmForm
     */
    function create();
}
