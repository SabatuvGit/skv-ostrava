<?php

namespace App\Components\FundEditForm;

use App\Components\BaseControl;
use App\Model\Entity\FundsEntity;
use App\Model\FundsModel;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

/**
 * Description of FundEditForm
 *
 * @author sabat
 */
class FundEditForm extends BaseControl {

    /**
     * @var array 
     */
    private $defaults;

    /**
     * @var int 
     */
    private $fundId;

    /**
     * @var FundsModel 
     */
    private $fundModel;

    public function __construct(FundsModel $fundModel) {
        $this->fundModel = $fundModel;
    }

    public function render() {
        $this->template->render(__DIR__ . "/templates/fundEdit.latte");
    }

    public function setDefaults($values) {
        $this->defaults = $values;
    }

    public function setFundId($fundId) {
        $this->fundId = $fundId;
    }

    public function createComponentEditForm() {
        $form = new Form();

        $form->addHidden('id',  $this->fundId);
        
        $form->getElementPrototype()->class('form-horizontal');
        $form->addSelect('type', 'Kategorie:', FundsEntity::getTypeArray());

        $form->addText('fund_name', 'Název: ')
                ->setRequired('Tohle pole je vyžadováno');
        $form->addText('link', 'Odkaz: ');

        if (isset($this->fundId)) {
            $fund = $this->fundModel->getFund($this->fundId);
            if ($fund) {
                $form->setDefaults($fund->toArray());
            }
            $submit = $form->addSubmit('create', 'Uložit');
        } else {
            $submit = $form->addSubmit('create', 'Vytvořit');
        }
        $submit->setAttribute('class', 'btn btn-success');

        $form->onSuccess[] = $this->formSuccess;

        // Add external success actions
        foreach ($this->onSuccess AS $action) {
            $form->onSuccess[] = $action;
        }
        if (!empty($this->defaults)) {
            $form->setDefaults($this->defaults);
        }

        return $form;
    }
    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSuccess(Form $form,ArrayHash $values) {
        if ($this->fundId) {
            $this->fundModel->updateValues($values);
        } else {
            $this->fundModel->createFund($values);
        }
    }

}

interface IFundEditForm {

    /**
     * @return FundEditForm
     */
    public function create();
}
