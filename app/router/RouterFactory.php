<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;

class RouterFactory {

    /**
     * @return Nette\Application\IRouter
     */
    public static function createRouter() {
        $router = new RouteList;
        $router[] = new Route('index.php', 'Front:Homepage:default', Route::ONE_WAY);
        
        $router[] = self::getAdmin();
        $router[] = self::getFront();

        return $router;
    }
    
    private static function getFront() {
        $router = new RouteList('Front');
        
        /*$router[] = new Route('kontakt/[<action>]', 'Contact:default');

        $router[] = new Route('<pageAlias (ostatni-akce|o-nas|projekty-eu)>/', 'Page:default');
*/
        
        $router[] = new Route('<pageAlias (dotace)>/', 'Funds:donations');
        $router[] = new Route('<pageAlias (nadace)>/', 'Funds:foundations');
        $router[] = new Route('<pageAlias (sponzori)>/', 'Funds:sponsors');
        $router[] = new Route('<presenter>/<action>/[<id>/]', 'Homepage:default');
        
        return $router;
    }
    
    private static function getAdmin() {
        $router = new RouteList('Admin');
        
        $router[] = new Route('admin/<presenter>/[<action>/][<id>/]', 'Dashboard:default');
        
        return $router;
    }

}
