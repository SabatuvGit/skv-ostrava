<?php

namespace App\FrontModule\Presenters;

use Nette;
use App\Model\FundsModel;

class SponsorsPresenter extends BasePresenter
{
    /**
     * @inject
     * @var FundsModel
     */
    public $fundsModel;

    public function actionDefault($fundsType) {

        $page = $this->fundsModel->getFundsList($fundsType);

        if (!$page) {
            throw new BadRequestException('Page with alias [' . $fundsType . '] not found');
        }
        $this->template->sponsors = $page;
    }
}
