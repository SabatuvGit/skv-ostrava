-- Adminer 4.2.2fx MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `articles` (`id`, `alias`, `title`, `content`) VALUES
(1,	'o-nas',	'About us',	'<h2>O n&aacute;s</h2>\r\n<p>V&iacute;t&aacute;me V&aacute;s na str&aacute;nk&aacute;ch Sportovn&iacute;ho klubu voz&iacute;čk&aacute;řů Ostrava, spolek . Sportovn&iacute; klub voz&iacute;čk&aacute;řů Ostrava (SKV) vznikl v roce 1994 a nav&aacute;zal č&aacute;stečně na činnost odd&iacute;lu TJ Meta Hrabyně. SKV Ostrava m&aacute; k dne&scaron;n&iacute;mu dni 30 členů. D&aacute; se ř&iacute;ct, že klub je v&scaron;estranně zaměřen&yacute;. V oblasti sportu se specializuje předev&scaron;&iacute;m na stoln&iacute; tenis handicapovan&yacute;ch sportovců, d&aacute;le poř&aacute;d&aacute; odborn&eacute; semin&aacute;ře z oblasti rehabilitace, zdravotnictv&iacute; a soci&aacute;ln&iacute; sf&eacute;ry.</p>\r\n<p>Odd&iacute;l několikr&aacute;t ročně poř&aacute;d&aacute; v&iacute;kendov&eacute; sportovn&iacute; soustředěn&iacute;, kter&eacute; je určeno předev&scaron;&iacute;m handicapovan&yacute;m hr&aacute;čům. Tato setk&aacute;n&iacute; se pravidelně kon&aacute; v arealu TJ Ostrava, V&aacute;renska 40a . Aby takov&aacute; to akce mohla proběhnout, je vhodn&eacute;, aby objekt i okol&iacute; vyhovovalo potřeb&aacute;m handicapovan&yacute;ch lid&iacute;. Soustředěn&iacute; se &uacute;častn&iacute; hr&aacute;či SKV Ostrava, sportovci z jin&yacute;ch odd&iacute;lů a reprezentanti ČR. Tak&eacute; zde zav&iacute;taj&iacute; mlad&iacute; zač&iacute;naj&iacute;c&iacute; hr&aacute;či, aby si ověřili sv&eacute; schopnosti. Tr&eacute;ninky prob&iacute;haj&iacute; dvouf&aacute;zově. Př&iacute;tomni jsou i sparing partneři z jin&yacute;ch sportovn&iacute;ch odd&iacute;lů. Sportovci maj&iacute; k dispozici odborn&yacute; rehabilitačn&iacute; person&aacute;l a fyzioterapeuta.</p>\r\n<p>Pro handicapovan&eacute; lidi z Ostravy a okol&iacute; SKV Ostrava jednou ročně poř&aacute;d&aacute; rekondičn&iacute; pobyt, kter&yacute; je zaměřen předev&scaron;&iacute;m na rehabilitaci, na předn&aacute;&scaron;ky o zdrav&eacute;m životn&iacute;m stylu, zabrous&iacute; se i do soci&aacute;ln&iacute; sf&eacute;ry, aby klienti byli průběžně informovan&iacute; o novink&aacute;ch a změn&aacute;ch v soci&aacute;ln&iacute; politice.</p>\r\n<p>Stalo se tradic&iacute;, že Sportovn&iacute; klub voz&iacute;čk&aacute;řů Ostrava poř&aacute;d&aacute; Mistrovstv&iacute; Česk&eacute; republiky ve stoln&iacute;m tenise voz&iacute;čk&aacute;řů. &Uacute;častnici jsou nominov&aacute;ni na z&aacute;kladě dev&iacute;ti nominačn&iacute;ch turnajů, kter&eacute; se konaj&iacute; během roku v nejrůzněj&scaron;&iacute;ch městech ČR jako Česk&yacute; poh&aacute;r. Na reprezentanty dohl&iacute;ž&iacute; během z&aacute;pasů takt&eacute;ž l&eacute;kařsk&yacute; a rehabilitačn&iacute; person&aacute;l.</p>\r\n<p>&nbsp;</p>'),
(3,	'contact',	'Kontakt',	'<h2>Kontakt</h2>\r\n<p><strong>Veden&iacute; klubu:</strong></p>\r\n<p>Radek Skyba &ndash; předseda klubu</p>\r\n<p>Ale&scaron; Ad&aacute;mek &ndash; m&iacute;stopředseda</p>\r\n<p>Be&aacute;ta Mentznerov&aacute; &ndash; m&iacute;stopředseda</p>\r\n<p>Anton&iacute;n Hora - m&iacute;stopředseda</p>\r\n<p>&nbsp;</p>\r\n<p><strong> Adresa:</strong></p>\r\n<p><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"font-size: 12.0pt; font-family: \'Times New Roman\',\'serif\'; mso-fareast-font-family: \'Times New Roman\'; mso-ansi-language: CS; mso-fareast-language: CS; mso-bidi-language: AR-SA;\">Sportovn&iacute; klub voz&iacute;čk&aacute;řů Ostrava, spolek</span></strong></p>\r\n<p>Ostrava, Moravsk&aacute; Ostrava a Př&iacute;voz, V&aacute;renska 3098/40a</p>\r\n<p>&nbsp;</p>\r\n<p><strong> E-mail:</strong> skv.ostrava@seznam.cz</p>\r\n<p><strong>Mobil:</strong> +420 603 162 933</p>\r\n<p><strong>IČ:</strong> 63025582</p>\r\n<p><strong>Bankovn&iacute; spojen&iacute;:</strong> ČSOB Ostrava</p>\r\n<p><strong>Č&iacute;slo &uacute;čtu:</strong> 102261885/0300</p>'),
(4,	'ostatni-akce',	'Ostatní akce',	'<p>Bla bla bla. Lorem ipsum dolor sit amet...</p>'),
(5,	'projekty-eu',	'Projekty EU',	'<p>projektytttttt</p>');

DROP TABLE IF EXISTS `donations`;
CREATE TABLE `donations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE ucs2_czech_ci NOT NULL,
  `link` varchar(255) COLLATE ucs2_czech_ci NOT NULL,
  `stored` varchar(255) COLLATE ucs2_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_czech_ci;

INSERT INTO `donations` (`id`, `name`, `link`, `stored`) VALUES
(1,	'Krajský úřad Moravskoslezského kraje',	'https://moap.ostrava.cz/cs',	'menici_logo1.png'),
(2,	'Statutární město Ostrava-městský obvod Ostrava jih',	'http://www.ostrava.cz/',	'menici_logo2.png'),
(3,	'Statutární město Ostrava-městský obvod Moravská Ostrava a Přívoz',	'http://www.kr-moravskoslezsky.cz/',	'menici_logo3.png'),
(4,	'Statutární město Ostrava - Poruba',	'http://www.moporuba.cz/cs/',	'menici_logo4.png'),
(5,	'Město Klimkovice',	'http://www.mesto-klimkovice.cz/',	'menici_logo5.png'),
(6,	'Magistrát města Karviná',	'http://www.karvina.cz/portal/page/portal/uvodni_stranka',	'menici_logo6.png'),
(7,	'Statutární město Havířov',	'http://www.havirov-city.cz/',	'menici_logo7.png'),
(8,	'Financováno z rozpočtu statutárního města Ostravy',	'http://www.ostrava.cz/cs',	'menici_logo8.png');

DROP TABLE IF EXISTS `funds`;
CREATE TABLE `funds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE latin2_czech_cs NOT NULL,
  `link` varchar(255) COLLATE latin2_czech_cs NOT NULL,
  `stored` varchar(255) COLLATE latin2_czech_cs NOT NULL,
  `type` enum('dotace','nadace','sponzori') COLLATE latin2_czech_cs NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

INSERT INTO `funds` (`id`, `name`, `link`, `stored`, `type`) VALUES
(1,	'Krajský úřad Moravskoslezského kraje',	'https://moap.ostrava.cz/cs',	'menici_logo1.png',	'dotace'),
(2,	'Statutární město Ostrava-městský obvod Ostrava jih',	'http://www.ostrava.cz/',	'menici_logo2.png',	'dotace'),
(3,	'Statutární město Ostrava-městský obvod Moravská Ostrava a Přívoz',	'http://www.kr-moravskoslezsky.cz/',	'menici_logo3.png',	'dotace'),
(4,	'Statutární město Ostrava - Poruba',	'http://www.moporuba.cz/cs/',	'menici_logo4.png',	'dotace'),
(5,	'Město Klimkovice',	'http://www.mesto-klimkovice.cz/',	'menici_logo5.png',	'dotace'),
(6,	'Magistrát města Karviná',	'http://www.karvina.cz/portal/page/portal/uvodni_stranka',	'menici_logo6.png',	'dotace'),
(7,	'Statutární město Havířov',	'http://www.havirov-city.cz/',	'menici_logo7.png',	'dotace'),
(8,	'Financováno z rozpočtu statutárního města Ostravy',	'http://www.ostrava.cz/cs',	'menici_logo8.png',	'dotace'),
(9,	'Nadační fond AVAST',	'http://www.avast.com/cs-cz/foundation',	'FOND_AVAST.png ',	'nadace'),
(10,	'Nadace Charty 77',	'http://www.kontobariery.cz/nadace/Nadace-Charty-77.aspx',	'Nadace Charty 77.jpg ',	'nadace'),
(11,	'Nadace OKD',	'http://www.nadaceokd.cz/cs',	'Nadace OKD.jpg',	'nadace'),
(12,	'NF Generali',	'http://www.generali.cz/',	'NF Generali logo_2014.jpg ',	'nadace'),
(13,	'Lasvit',	'http://lasvitfoundation.com/',	'Partner-logo_LASVIT_b.jpg ',	'nadace'),
(14,	'http://www.rwe.cz/',	'http://www.rwe.cz/',	'RWE.jpg',	'sponzori'),
(15,	'http://gmctech.cz/',	'http://gmctech.cz/',	'menici_logo36.png',	'sponzori'),
(16,	'http://www.bmwcartec.cz/',	'http://www.bmwcartec.cz/',	'CarTec_Group.jpg',	'sponzori'),
(17,	'http://www.motorlucina.cz/cz/index.php',	'http://www.motorlucina.cz/cz/index.php',	'image003.gif',	'sponzori'),
(18,	'http://www.secotools.com/cz',	'http://www.secotools.com/cz',	'menici_logo38.png',	'sponzori'),
(19,	'http://www.kovoklima.cz/',	'http://www.kovoklima.cz/',	'Kovo Nerez1.jpg',	'sponzori'),
(20,	'http://www.strojirny.com/',	'http://www.strojirny.com/',	'menici_logo39.png',	'sponzori'),
(21,	'http://www.elkoplast.cz/',	'http://www.elkoplast.cz/',	'logo-Elkoplast_RGB_nahled_72DPI.jpg',	'sponzori'),
(22,	'http://emco.cz/',	'http://emco.cz/',	'EMCO_refresh_LOGO_small.jpg',	'sponzori'),
(23,	'http://www.jagermeister.cz',	'http://www.jagermeister.cz',	'Jägermeister1.jpg',	'sponzori'),
(24,	'http://www.konczanka.cz/',	'http://www.konczanka.cz/',	'Konczanka logo 2010 (2).jpg',	'sponzori'),
(25,	'http://www.lesycr.cz/',	'http://www.lesycr.cz/',	'Lesy CR.jpg',	'sponzori'),
(26,	'http://www.nutrend.cz/',	'http://www.nutrend.cz/',	'Nutrend.jpg',	'sponzori'),
(27,	'http://www.bonatrans.cz/',	'http://www.bonatrans.cz/',	'bonatrans.jpg',	'sponzori'),
(28,	'http://www.cesky.porcelan.cz/',	'http://www.cesky.porcelan.cz/',	'cesky porcelán.jpg',	'sponzori'),
(29,	'http://www.koop.cz/',	'http://www.koop.cz/',	'menici_logo11.png',	'sponzori'),
(30,	'http://www.lenzing.com/en',	'http://www.lenzing.com/en',	'menici_logo12.png',	'sponzori'),
(31,	'http://www.anect.com/cz',	'http://www.anect.com/cz',	'menici_logo13.png',	'sponzori'),
(32,	'http://www.printo.cz/',	'http://www.printo.cz/',	'menici_logo14.png',	'sponzori'),
(33,	'http://www.panter-praha.cz/',	'http://www.panter-praha.cz/',	'menici_logo15.png',	'sponzori'),
(34,	'http://www.nh-trans.cz/',	'http://www.nh-trans.cz/',	'menici_logo16.png',	'sponzori'),
(35,	'http://www.mgog.cz/',	'http://www.mgog.cz/',	'menici_logo17.png',	'sponzori'),
(36,	'http://www.fite.cz/',	'http://www.fite.cz/',	'menici_logo19.png',	'sponzori'),
(37,	'http://www.ppl.cz/',	'http://www.ppl.cz/',	'menici_logo21.png',	'sponzori'),
(38,	'http://www.hayes-lemmerz.com',	'http://www.hayes-lemmerz.com',	'menici_logo22.png',	'sponzori'),
(39,	'http://www.diagnostikastroju.cz/',	'http://www.diagnostikastroju.cz/',	'menici_logo23.png',	'sponzori'),
(40,	'http://www.tandemgroup.cz/',	'http://www.tandemgroup.cz/',	'menici_logo24.png',	'sponzori'),
(41,	'http://www.urspraha.cz/',	'http://www.urspraha.cz/',	'menici_logo25.png',	'sponzori'),
(42,	'http://www.cegelec.cz/',	'http://www.cegelec.cz/',	'menici_logo27.png',	'sponzori'),
(43,	'http://www.mohruska.cz/',	'http://www.mohruska.cz/',	'menici_logo28.png',	'sponzori'),
(44,	'http://www.satum.cz/',	'http://www.satum.cz/',	'menici_logo30.png',	'sponzori'),
(45,	'http://www.glass.cz/',	'http://www.glass.cz/',	'menici_logo31.png',	'sponzori'),
(46,	'http://www.alpisport.cz/',	'http://www.alpisport.cz/',	'Alpisport1.jpg',	'sponzori'),
(47,	'http://www.eurovia.cz/',	'http://www.eurovia.cz/',	'menici_logo41.png',	'sponzori'),
(48,	'http://www.syner.cz/',	'http://www.syner.cz/',	'syner.jpg',	'sponzori'),
(49,	'http://www.veba.cz/',	'http://www.veba.cz/',	'veba.jpg',	'sponzori'),
(50,	'http://www.albatrosmedia.cz/',	'http://www.albatrosmedia.cz/',	'ALBATROS_MEDIA_CMYK.jpg',	'sponzori'),
(51,	'http://www.centr.cz/',	'http://www.centr.cz/',	'centr.png',	'sponzori');

DROP TABLE IF EXISTS `photogallery`;
CREATE TABLE `photogallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `image` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `starting_date` date NOT NULL,
  `ending_date` date NOT NULL,
  `stored_dir` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;

INSERT INTO `photogallery` (`id`, `name`, `image`, `starting_date`, `ending_date`, `stored_dir`) VALUES
(1,	'Brno',	'IMG_0903.JPG',	'2015-11-07',	'0000-00-00',	'Brno'),
(2,	'Mistrovství české republiky ve stolním tenise vozíčkářů Ostrava',	'MCR_2010.jpg',	'2010-05-31',	'2010-06-03',	'MCR-2010');

-- 2015-12-09 21:28:36
