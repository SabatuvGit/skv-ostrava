<?php

namespace App\Console;

use App\Components\FtpDeployer;
use Nette\FileNotFoundException;
use Nette\Neon\Neon;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class FtpDeployCommand extends Command {

	protected function configure() {
		$this->setName('deploy:ftp')
			->setDescription('Deploys apliaction to FTP server')
			->addArgument('password', InputArgument::REQUIRED, 'FTP Password');
	}

	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @SuppressWarnings("PMD.UnusedFormalParameter")
	 * @return void
	 */
	protected function execute(InputInterface $input, OutputInterface $output) {
		$password = $input->getArgument('password');

		$configFile = file_get_contents(APP_DIR . "/config/deployment.neon");

		if (!$configFile) {
			throw new FileNotFoundException('File "deployment.neon" not found.');
		}

		$configWeb = Neon::decode($configFile);
		$configWeb += [
			'remote' => 'ftp://' . $configWeb['ftpUser'] . ':' . $password . '@' . $configWeb['ftpServer'],
		];

		$config = array(
			$configWeb['ftpServer'] => $configWeb,
			'tempdir' => TEMP_DIR,
			'colors' => False,
		);

		$deployer = new FtpDeployer($config);

		$deployer->run();
	}

}
