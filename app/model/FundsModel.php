<?php

namespace App\Model;

use App\Model\Entity\FundsEntity;
use App\Model\Entity\PageEntity;

/**
 * Description of FundsModel
 *
 * @author sabat
 */
class FundsModel extends BaseModel {

    /**
     * @var String
     */
    const TABLE_NAME = 'funds';

    /**
     * @var FundsEntity 
     */
    private $lastResult = NULL;

    /**
     * @param string $fundsType
     * @return bool|FundsEntityArray
     */
    public function getFundsList($fundsType) {

        $sql = "SELECT * FROM funds "
                . "WHERE type = ?";
        $funds = $this->connection->queryArgs($sql, array($fundsType))
                ->fetchAll();

        $fundsArray = array();
        foreach ($funds as $row) {
            $fundsArray[] = new FundsEntity($row);
        }
        if (!empty($fundsArray)) {
            return $fundsArray;
        }
        return false;
    }

    /**
     * @param string $fundsType
     * @return bool|FundsEntityArray
     */
    public function getFundsListWithImage($fundsType) {

        $sql = "SELECT funds.id,fund_name,link,type,funds.order,image.id as image_id,image.name,image.format FROM funds,image "
                . "WHERE funds.image_id = image.id "
                . "AND type = ? ORDER BY funds.order ASC";
        $funds = $this->connection->queryArgs($sql, array($fundsType))
                ->fetchAll();
        $fundsArray = array();
        foreach ($funds as $row) {
            $image = new Entity\ImageEntity($row);
            $fund = new FundsEntity($row);
            $fund->setImageObj($image);
            $fundsArray[] = $fund;
        }
        if (!empty($fundsArray)) {
            return $fundsArray;
        }
        return false;
    }

    public function getNoLogoFundsList($fundsType) {
        $sql = "SELECT * FROM funds "
                . "WHERE funds.image_id is NULL AND type = ? ORDER BY funds.order ASC";
        $results = $this->connection->queryArgs($sql, array($fundsType))
                ->fetchAll();
        $funds = array();
        foreach ($results as $result) {
            $funds[] = new FundsEntity($result);
        }
        if (!empty($funds)) {
            return $funds;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param int $fundId
     * @return FundsEntity,boolean
     */
    public function getFundWithImage($fundId) {
        if (isset($this->lastResult) and ( $this->lastResult->getId() == $fundId)) {
            return $this->lastResult;
        } else {
            $sql = "SELECT funds.id,fund_name,link,type,image.id as image_id,name,format FROM funds,image "
                    . "WHERE funds.image_id = image.id "
                    . "AND funds.id = ?";
            $result = $this->connection->queryArgs($sql, array($fundId))
                    ->fetch();
            if ($result) {
                $image = new Entity\ImageEntity($result);
                $fund = new FundsEntity($result);
                $fund->setImageObj($image);
                $this->lastResult = $fund;
                return $fund;
            } else {
                return false;
            }
        }
    }

    /**
     * @param int $fundId
     * @param int $fundType
     */
    public function orderUp($fundId, $fundType) {

        $fund = $this->getFund($fundId);
        $fundOrderNo = $fund->order;

        $upperFund = $this->getFundByOrderNo($fund->order - 1, $fundType);
        $this->setOrder($fundId, $fundOrderNo - 1);
        $this->setOrder($upperFund->id, $fundOrderNo);
    }

    /**
     * @param int $fundId
     * @param int $fundType
     */
    public function orderDown($fundId, $fundType) {

        $fund = $this->getFund($fundId);
        $fundOrderNo = $fund->order;

        $lowerFund = $this->getFundByOrderNo($fund->order + 1, $fundType);
        $this->setOrder($fundId, $fundOrderNo + 1);
        $this->setOrder($lowerFund->id, $fundOrderNo);
    }

    /**
     * @param int $fundId
     * @param int $orderNo
     */
    public function setOrder($fundId, $orderNo) {
        $fund = $this->connection->table(self::TABLE_NAME)->where('id', $fundId);
        $fund->update(array(
            'order' => $orderNo,
        ));
    }

    /**
     * @param int $fundOrderNo
     * @param int $fundType
     * @return FundsEntity|boolean
     */
    private function getFundByOrderNo($fundOrderNo, $fundType) {
        $sql = "SELECT * FROM funds WHERE funds.order = ? AND funds.type = ?";
        $fund = $this->connection->queryArgs($sql, array($fundOrderNo, $fundType))
                ->fetch();
        if ($fund) {
            return new FundsEntity($fund);
        }
        return False;
    }

    /**
     * @param int ID
     * @return bool|PageEntity
     */
    public function getFund($id) {
        $sql = "SELECT * FROM funds WHERE id = ?";
        $fund = $this->connection->queryArgs($sql, array($id))
                ->fetch();
        if ($fund) {
            return new FundsEntity($fund);
        }
        return False;
    }

    public function setBanner($imageId, $fundId) {
        $fund = $this->getFund($fundId);
        $maxValue = $this->getMaxOrderNo($fund->type);
        $fundUpdate = $this->connection->table(self::TABLE_NAME)->where('id', $fundId);
        if (NULL !== $fund->getImageId()) {
            $fundUpdate->update(array(
                'image_id' => $imageId,
            ));
        }else {
            $fundUpdate->update(array(
                'image_id' => $imageId,
                'order' => $maxValue + 1,
            ));
        }
    }

    public function getMaxOrderNo($fundType) {
        //SELECT MAX(column_name) FROM table_name;
        $sql = "SELECT MAX(funds.order) FROM funds where funds.type = ?";
        $maxValue = $this->connection->queryArgs($sql, array($fundType))
                ->fetch();
        if ($maxValue) {
            return $maxValue['MAX(funds.order)'];
        }
        return False;
    }

    public function createFund($values) {
        $this->connection->table(self::TABLE_NAME)->insert($values);
    }

    public function updateValues($values) {
        $page = $this->connection->table(self::TABLE_NAME)->where('id', $values->id);
        $page->update($values);
    }

    public function removeFund($id) {
        $this->connection->table(self::TABLE_NAME)->where('id',$id)->delete();
    }

    /**
     * 
     * @param int $fundsType
     * @return string, bool
     */
    public function getTitle($fundsType) {
        return FundsEntity::getTypeName($fundsType);
    }

}
