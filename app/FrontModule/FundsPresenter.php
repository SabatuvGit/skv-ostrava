<?php

namespace App\FrontModule\Presenters;

use App\Model\FundsModel;

/**
 * Description of FundsPresenter
 *
 * @author sabat
 */
class FundsPresenter extends BasePresenter {

    /**
     * @inject
     * @var FundsModel
     */
    public $fundsModel;

    public function actionDonations($fundsType) {

        $page = $this->fundsModel->getFundsListWithImage(1);
        if (!$page) {
            $this->template->message = 'Momentálně nedostáváme žádné dotace.';
        } else {
            $this->template->donations = $page;
        }
    }

    public function actionFoundations($fundsType) {

        $page = $this->fundsModel->getFundsListWithImage(2);
        if (!$page) {
            $this->template->message = 'Momentálně nemáme žádného sponzora.';
        } else {
            $this->template->foundations = $page;
        }
    }

    public function actionSponsors($fundsType) {

        $page = $this->fundsModel->getFundsListWithImage(3);
        if (!$page) {
            $this->template->message = 'Momentálně nejsme financování z žádné nadace.';
        } else {
            $this->template->sponsors = $page;
        }
    }

}
