<?php

namespace App\FrontModule\Presenters;

use Nette\Utils\Html;

class HomepagePresenter extends BasePresenter {

    public function renderDefault($pageAlias) {
        $el = Html::el('img'); // vytvoří element <img>
        $el->src = '/images/main_img.jpg'; // nastaví atribut src
        $el->class = 'img-responsive center-block';
        $el->width = '1200';
        $this->template->source = $el;
    }

}
