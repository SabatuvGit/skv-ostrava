let $ = require('jquery');

/**
 * @param {string} selector
 * @param {string} className
 */
let dropdown = function(selector, className = 'open') {
    let element = $(selector);
    /*let isOpen = false;

    element.on('click', function() {
        if(isOpen){
            $(this).removeClass(className);
            isOpen = false;
        }else{
            $(this).addClass(className);
            isOpen = true;
        }
    });*/

    /*element.on('click', function() {
        if($(this).hasClass(className)) {
            $(this).removeClass(className);
        } else {
            $(this).addClass(className);
        }
    });*/

    element.on('click', function() {
        $(this).toggleClass(className);
    });
};

module.exports = dropdown;
