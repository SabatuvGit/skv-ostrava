<?php

namespace App\AdminModule\Presenters;

use App\Components\PageEditForm\IPageEditForm;
use App\Model\PageModel;
use Nette\Application\BadRequestException;
use Nette\Forms\Form;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

/**
 * Description of EditPagePresenter
 *
 * @author sabat
 */
class PagePresenter extends BasePresenter {

    /**
     * @inject
     * @var PageModel
     */
    public $pageModel;


    public function renderDefault() {

        $pages = $this->pageModel->getAllPages();
        $this->template->pages = $pages;
    }

    public function actionEdit($pageId) {
        $page = $this->pageModel->getPage($pageId);

        if (!$page) {
            throw new BadRequestException('Page with id [' . $pageId . '] not found');
        }
        $this->template->page = $page;
        
        $this['pageEditForm']->setDefaults($page->toArray());
    }

    public function actionResult($pageAlias) {

        $page = $this->pageModel->getPageByAlias($pageAlias);

        if (!$page) {
            throw new BadRequestException('Page with alias [' . $pageAlias . '] not found');
        }
        $this->template->page = $page;
    }

    /**
     * @return Form
     */
    public function createComponentPageEditForm() {

        $form = $this->context->getByType(IPageEditForm::class)->create();

        $form-> addSuccess($this->editPageSucceeded);
        
        return $form;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Form $form
     * @param ArrayHash $values
     */
    public function editPageSucceeded($form, $values) {
        $this->flashMessage('Tahnle vypadají provedené změny.', 'alert alert-success');
        
        $this->redirect('Page:result', $values->alias);
    }

}
