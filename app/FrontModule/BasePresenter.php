<?php

namespace App\FrontModule\Presenters;

use Nette\Application\UI\Presenter;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Presenter {

    /**
     * @persistent
     */
    public $mobile;

    public function beforeRender() {
        parent::beforeRender();

        $this->template->dateFormat = $this->context->parameters['dateFormat'];

        $this->template->menuItems = array(
            'O nás' => array(
                'path' => 'Page:',
                'params' => 'o-nas',
            ),
            'Akce SKV' => array(
                'path' => 'Page:',
                'params' => 'actions-skv',
                'subMenu' => array(
                    'MCR 2015' => array(
                        'path' => 'Page:',
                        'params' => 'MCR-2015',
                    ),
                    'MCR 2014' => array(
                        'path' => 'Page:',
                        'params' => 'MCR-2014',
                        'name' => 'MCR 2014',
                    ),
                    'MCR 2013' => array(
                        'path' => 'Page:',
                        'params' => 'MCR-2013',
                        'name' => 'MCR 2013',
                    ),
                    'MCR 2012' => array(
                        'path' => 'Page:',
                        'params' => 'MCR-2012',
                        'name' => 'MCR 2012',
                    ),
                    'Ostatníakce' => array(
                        'path' => 'Page:',
                        'params' => 'ostatni-akce',
                    ),
                ),
            ),
            'Fotogalerie' => array(
                'path' => 'Photogallery:',
            ),
            'Dotace' => array(
                'path' => 'Funds:donations',
                'params' => 'dotace',
            ),
            'Nadace' => array(
                'path' => 'Funds:foundations',
                'params' => 'nadace',
            ),
            'Sponzoři' => array(
                'path' => 'Funds:sponsors',
                'params' => 'sponzori',
            ),
            'Projekty EU' => array(
                'path' => 'Page:',
                'params' => 'projekty-eu',
                'subMenu' => array(
                    'PROD' => array(
                        'path' => 'Page:',
                        'params' => 'PROD',
                        'name' => 'PROD',
                    ),
                    'MODEL' => array(
                        'path' => 'Page:',
                        'params' => 'MODEL',
                        'name' => 'MODEL',
                    ),
                ),
            ),
            'Kontakt' => array(
                'path' => 'Page:',
                'params' => 'contact',
            ),
        );
    }

}
