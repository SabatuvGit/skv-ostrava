<?php

namespace App\Model\Entity;

use Nette\Database\Row;
use Nette\Object;

/**
 * Description of BaseEntity
 *
 * @author sabat
 */
abstract class BaseEntity extends Object {

        /**
     * @var Row
     */
    protected $data;

    /**
     * @param Row $data
     * @param string $dateFormat
     */
    public function __construct(Row $data) {

        $this->data = $data;
    }

}
