<?php

namespace App\AdminModule\Presenters;

use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter {

    public function startup() {
        parent::startup();

        if (!$this->user->loggedIn) {
            $this->flashMessage('Před vstupem do administrace musíte být přihlášený');

            $backlink = $this->storeRequest();

            $this->redirect(':Front:Sign:in', array(
                'backlink' => $backlink,
            ));
        } else {
            $this->template->user = $this->user->id;
        }
    }

    public function beforeRender() {
        parent::beforeRender();
        
        if($this->isAjax()) {
            $this->setLayout(false);
        }

        $this->template->dateFormat = $this->context->parameters['dateFormat'];
        
         $this->template->isAjax = $this->isAjax();

        $this->template->menuAdminItems = array(
            'Obsah stránek' => array(
                'path' => 'Page:',
            ),
            'Přispívající organizace' => array(
                'path' => 'Funds:',
                'subMenu' => array(
                    'Dotace' => array(
                        'path' => 'Funds:',
                        'params' => '1',
                    ),
                    'Nadace' => array(
                        'path' => 'Funds:',
                        'params' => '2',
                    ),
                    'Sponzori' => array(
                        'path' => 'Funds:',
                        'params' => '3'
                    ),
                ),
            ),
            'Fotogalerie' => array(
                'path' => 'Photogallery:',
            ),
            'Fotky' => array(
                'path' => 'Photos:default',
            ),
        );
    }

    protected function isSuperuser() {
        return in_array('superadmin', $this->user->roles);
    }

}
