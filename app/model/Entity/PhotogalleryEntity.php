<?php

namespace App\Model\Entity;

use Nette\Database\Row;

/**
 * Description of PhotogalleryEntity
 *
 * @author sabat
 */
class PhotogalleryEntity extends BaseEntity {

    const Photogallery = 1;
    const ArticleGallery = 2;

    /**
     * @var Array
     */
    protected static $TYPE = array(
        1 => 'Photogallery',
        2 => 'Page',
        3 => 'Fund',
    );

    /**
     * @var ImageEntity 
     */
    private $image = NULL;

    /**
     * @var string
     */
    private $dateFormat;

    /**
     * @param Row $data
     * @param string $dateFormat
     */
    public function __construct(Row $data, $dateFormat) {
        $this->data = $data;

        $this->dateFormat = $dateFormat;
    }

    /**
     * @return String
     */
    public function getDateAsString() {
        $returnStr = $this->data->starting_date->format($this->dateFormat);

        if ($this->data->starting_date != $this->data->ending_date) {
            $returnStr .=" - " . date_format($this->data->ending_date, $this->dateFormat);
        }

        return $returnStr;
    }

    /**
     * @return Date
     */
    public function getStartingDate() {
        return date_format($this->data->starting_date, $this->dateFormat);
    }

    /**
     * @return Date,bool
     */
    public function getEndingDate() {
        if ($this->data->starting_date != $this->data->ending_date) {
            return date_format($this->data->ending_date, $this->dateFormat);
        }
        return false;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->data->gallery_id;
    }

    /**
     * @return String
     */
    public function getTitle() {
        return $this->data->title;
    }

    /**
     * 
     * @return int
     */
    public function getType() {
        return $this->data->type;
    }

    /**
     * @return string
     */
    public function getTypeString() {
        $title = self::$TYPE;
        return $title[$this->getType()];
    }

    /**
     * @param int
     * @return String , bool
     */
    public static function getTypeName($galleryNo) {
        if (array_key_exists($galleryNo, self::$TYPE)) {
            $title = self::$TYPE;
            return $title[$galleryNo];
        } else {
            return false;
        }
    }

    /**
     * @return Array
     */
    public static function getTypeArray() {
        return self::$TYPE;
    }

    /**
     * @return ImageEntity | NULL
     */
    public function getImageObj() {
        if (isset($this->image)) {
            return $this->image;
        } else {
            return NULL;
        }
    }

    public function getImgObjId() {
        return $this->data->welcome_image_id;
    }

    /**
     * @param ImageEntity $image
     */
    public function setImageObj(ImageEntity $image) {
        $this->image = $image;
    }

    /**
     * 
     * @return Row array
     */
    public function toArray() {
        $result = array(
            'gallery_id' => $this->getId(),
            'title' => $this->getTitle(),
            'starting_date' => $this->getStartingDate(),
            'ending_date' => $this->getEndingDate(),
            'welcome_image_id' => $this->getImgObjId(),
            'type' => $this->getType(),
        );
        return $result;
    }

}
