<?php

namespace App\Model;

use App\Model\Entity\PageEntity;
/**
 * Description of PageModel
 *
 * @author sabat
 */
class PageModel extends BaseModel {
    /**
     * @var String
     */
    const TABLE_NAME = 'pages';
    
    /**
     * @param string $pageAlias
     * @return bool|PageEntity
     */
    public function getAllPages() {
        $sql = "SELECT * FROM pages";
        $pages=$this->connection->query($sql)
                ->fetchAll();
        $pagesArray=array();
        foreach ($pages as $row){
            $pagesArray[] = new Entity\PageEntity($row);
        }
        if(!empty($pagesArray)){
            return $pagesArray;
        }
        return False;
    }
    
    /**
     * @param int ID
     * @return bool|PageEntity
     */
    public function getPage($id) {
        $sql = "SELECT * FROM pages WHERE id = ?";
        $page=$this->connection->queryArgs($sql,array($id))
                ->fetch();
        if ($page) {
            return new PageEntity($page);
        }
        return False;
    }
    
    /**
     * @param int ID
     * @return bool|PageEntity
     */
    public function getPageByAlias($alias) {
        $sql = "SELECT * FROM pages WHERE alias = ?";
        $page=$this->connection->queryArgs($sql,array($alias))
                ->fetch();
        if ($page) {
            return new PageEntity($page);
        }
        return False;
    }
    
    /**
     * 
     * @param array $values
     */
    public function updateValues($values){
        
        $page=$this->connection->table(self::TABLE_NAME)->where('id',$values->id);
        $page->update($values);
        
    }
}
