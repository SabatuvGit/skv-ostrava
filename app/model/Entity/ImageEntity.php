<?php

namespace App\Model\Entity;

use App\Model\ImagesModel;

/**
 * Description of FundsEntity
 *
 * @author sabat
 */
class ImageEntity extends BaseEntity {

    /**
     * @return int
     */
    public function getId() {

        return $this->data->id;
    }

    /**
     * @return int
     */
    public function getPhotogalleryId() {

        return $this->data->photogallery_id;
    }

    /**
     * @return string
     */
    public function getName() {

        return $this->data->name;
    }

    /**
     * @return string
     */
    public function getFormat() {
        return $this->data->format;
    }

    /**
    * @return string
    */
    public function getFullName() {
        return $this->getName() . '.' . $this->getFormat();
    }

    /**
     * @return Array
     */
    public function toArray() {
        return $this->data->toArray();
    }

    /**
     * @return string
     */
    public function getLinkThumbNail() {
        $size = ImagesModel::IMAGE_THUMBNAIL;
        return $this->makeLink($size[0], $size[1]);
    }

    /**
     * @return string
     */
    public function getLinkBig() {
        $size = ImagesModel::IMAGE_BIG;
        return $this->makeLink($size[0], $size[1]);
    }
    
    /**
     * @return string
     */
    public function getLinkIcon() {
        $size = ImagesModel::IMAGE_ICON;
        return $this->makeLink($size[0], $size[1]);
    }
    /**
     * @return string
     */
    public function getLinkBanner() {
        $size = ImagesModel::IMAGE_BANNER;
        return $this->makeLink($size[0], $size[1]);
    }
    /**
     * @return string
     */
    public function getLinkPreview() {
        $size = ImagesModel::IMAGE_PREVIEW;
        return $this->makeLink($size[0], $size[1]);
    }
    
    /**
     * @param int $width
     * @param int $height
     * @return string
     */
    private function makeLink($width, $height) {
        if ($width == NULL) {
            $link = "/data/" . $this->getName() . "_" . "auto" . "_" . $height . "_." . $this->getFormat();
        } elseif ($height == NULL) {
            $link = "/data/" . $this->getName() . "_" . $width . "_" . "auto" . "_." . $this->getFormat();
        } else {
            $link = "/data/" . $this->getName() . "_" . $width . "_" . $height . "_." . $this->getFormat();
        }
        return $link;
    }

}
