<?php

namespace App\Components;

use Nette\Object;

/**
 * Description of SQLBatchOperator
 *
 * @author sabat
 */
class SQLBatchOperator extends Object {

    /**
     * @var string
     */
    private $server;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $dbName;

    public function __construct($server, $user, $password, $dbName) {
        $this->server = $server;
        $this->user = $user;
        $this->password = $password;
        $this->dbName = $dbName;
    }

    private function getMysqli() {
        return new \mysqli($this->server, $this->user, $this->password, $this->dbName);
    }
    
    public function export($filename = NULL) {
        $file = $filename ? $filename : $this->dbName . '.sql';
        
        $dump = new \MySQLDump($this->getMysqli());
        
        $dump->save($file);
        
        return $file;
    }

}
