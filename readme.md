Skv Ostrava
=============


Installing
----------
1. get into your projects directory

2. git clone https://SabatuvGit@bitbucket.org/SabatuvGit/skv-ostrava.git
   -- will create skv-ostrava directory

3. move into skv-ostrava directory and run
   sudo php composer.phar install 

4. change ownership of folder from root to your user
   sudo chown user skv-ostrava

5. change permission of /www for others - can read
   sudo chmod -R a+r www/

6. change permision of /www/data for others can read and write

7. change permission of /log and /temp to read and write
   sudo chmod -R a+rw log temp

Installing npm,bower,gulp

1. sudo apt-get update
   sudo apt-get install nodejs
   ln -s /usr/bin/nodejs /usr/bin/node

2. sudo apt-get install npm

3. sudo npm install -g bower

4. sudo npm install gulp -g

5. sudo npm install gulp-sass
   sudo npm install run-sequence

6. bower install

License
-------
- Nette: New BSD License or GPL 2.0 or 3.0 (https://nette.org/license)
- Adminer: Apache License 2.0 or GPL 2 (https://www.adminer.org)