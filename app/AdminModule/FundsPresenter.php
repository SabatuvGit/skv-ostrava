<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\AdminModule\Presenters;

use App\Components\FundEditForm\IFundEditForm;
use App\Components\RemoveConfirmForm\IRemoveConfirmForm;
use App\Components\UnusedImagesForm\IUnusedImagesForm;
use App\Components\WellcomeImg\IWellcomeImg;
use App\Model\Entity\FundsEntity;
use App\Model\FundsModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

/**
 * Description of EditFundsPresenter
 *
 * @author sabat
 */
class FundsPresenter extends BasePresenter {

    /**
     * @inject
     * @var FundsModel
     */
    public $fundsModel;

    /** @var IWellcomeImg @inject */
    public $wellcomeImgFactory;

    public function beforeRender() {
        parent::beforeRender();
        $this->template->editOrganizationItems = array(
            'Dotace' => array(
                'path' => 'Funds:',
                'params' => '1',
            ),
            'Nadace' => array(
                'path' => 'Funds:',
                'params' => '2',
            ),
            'Sponzoři' => array(
                'path' => 'Funds:',
                'params' => '3',
            ),
        );
    }

    public function renderDefault($fundsType) {

        $result = $this->fundsModel->getFundsListWithImage($fundsType);

        if (!$result) {
            $this->template->fundsType = $fundsType;
        } else {
            $this->template->fundsType = $fundsType;
            $this->template->title = $this->fundsModel->getTitle($fundsType);
            $this->template->type = $this->fundsModel->getTitle($fundsType);
            $this->template->foundations = $result;
        }

        $noLogo = $this->fundsModel->getNoLogoFundsList($fundsType);
        if ($noLogo) {
            $this->template->foundationsNologo = $noLogo;
        }
        $this->template->typeName = FundsEntity::getTypeName($fundsType);
    }

    public function renderNew($fundsType) {
        $this->template->title = $this->fundsModel->getTitle($fundsType);
    }

    public function actionEdit($fundId) {
        $fund = $this->fundsModel->getFundWithImage($fundId);
        if (!$fund) {
            $fund = $this->fundsModel->getFund($fundId);
            if (!$fund) {
                $this->error('Objekt nebyl nalezen');
            }
        }
        $this->template->fund = $fund;
    }

    public function actionselect($fundId) {
        $fund = $this->fundsModel->getFund($fundId);
        if ($fund) {
            $this->template->fundName = $fund->getFundName();
        }
    }

    public function actionMoveUp($fundId, $fundsType) {
        $this->fundsModel->orderUp($fundId, $fundsType);

        $this->redirect('Funds:default', $fundsType);
    }

    public function actionMoveDown($fundId, $fundsType) {
        $this->fundsModel->orderDown($fundId, $fundsType);

        $this->redirect('Funds:default', $fundsType);
    }

    public function actionRemove($fundId, $fundsType) {
        $fund = $this->fundsModel->getFundWithImage($fundId);
        if ($fund) {
            $this->template->fund = $fund;
            $this->template->fundTypeName = $this->fundsModel->getTitle($fundsType);
        } else {
            $this->error($this->fundsModel->getTitle($fundsType) . " nebyl(a) nalezen(a).");
        }
    }

    public function createComponentRemoveConfirmForm() {
        $fundId = $this->getParameter("fundId");
        $form = $this->context->getByType(IRemoveConfirmForm::class)->create();

        $form->addSuccess($this->removeSuccess);
        $form->setRelationEntityId($fundId);
        return $form;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Form $form
     * @param ArrayHash $values
     */
    public function removeSuccess(Form $form, ArrayHash $values) {
        $fundType = $this->getParameter('fundsType');
        if ($form['remove']->submittedBy) {
            $this->fundsModel->removeFund($values->id);
            $this->flashMessage($this->fundsModel->getTitle($fundType)." byl(a) úspěšně odstraněn(a).","alert alert-success");
            $this->redirect('Funds:default', $fundType);
        } elseif ($form['cancel']->submittedBy) {
            $this->redirect('Funds:default', $fundType);
        }
    }

    public function createComponentFundImages() {
        $form = $this->context->getByType(IUnusedImagesForm::class)->create();

        $form->setRelationEntityId(-1);

        $form->addSuccess($this->setImageToFund);

        return $form;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Form $form
     * @param ArrayHash $values
     */
    public function setImageToFund(Form $form, ArrayHash $values) {
        $imageId = $form->isSubmitted()->name;
        $fundId = $this->getParameter('fundId');

        $this->fundsModel->setBanner($imageId, $fundId);

        $this->redirect('Funds:edit', $fundId);
    }

    /**
     * @return Control WelcomeImg
     */
    public function createComponentWelcomeImgEdit() {

        $imgEdit = $this->context->getByType(IWellcomeImg::class)->create();

        $fundId = $this->getParameter('fundId');
        $imgEdit->setRelationParameterName('fundId');
        $fund = $this->fundsModel->getFundWithImage($fundId);
        if ($fund) {
            if (NULL !== $fund->getImageObj()) {
                $image = $fund->getImageObj();
                $imgEdit->setImageId($image);
            }
        }
        return $imgEdit;
    }

    /**
     * @param int $imageId
     * @param int $fundId
     */
    public function actionSetWelcomeImage($imageId, $fundId) {

        $this->fundsModel->setBanner($imageId, $fundId);

        $this->redirect('Funds:edit', $fundId);
    }

    /**
     * @return Form
     */
    public function createComponentCreateFundForm() {
        $form = $this->context->getByType(IFundEditForm::class)->create();

        $fundsType = $this->getParameter('fundsType');
        $form->setDefaults(array('type' => $fundsType));

        $form->addSuccess($this->createFundSucceeded);

        return $form;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Form $form
     * @param ArrayHash $values
     */
    public function createFundSucceeded(Form $form, ArrayHash $values) {
        
        $fundType = $values->type;
        $this->flashMessage('Nová ' . FundsEntity::getTypeName($values->type) . ' byl(a) úspěšně vytvořen(a)', 'alert alert-success');
        $this->redirect('Funds:default',$fundType);
    }

    public function createComponentFundEditForm() {
        $form = $this->context->getByType(IFundEditForm::class)->create();
        $form->setFundId($this->getParameter('fundId'));
        $form->addSuccess($this->editFundSucceeded);
        return $form;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Form $form
     * @param ArrayHash $values
     */
    public function editFundSucceeded(Form $form, ArrayHash $values) {
        $fundType = $values->type;
        $this->flashMessage('Změny byly úspěšně uloženy.', 'alert alert-success');

        $this->redirect('Funds:default',$fundType);
    }

}
