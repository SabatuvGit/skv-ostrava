<?php

namespace App\Components\PageEditForm;

use App\Components\BaseControl;
use App\Model\PageModel;
use Nette\Application\UI\Form;

/**
 * Description of PageEditForm
 *
 * @author sabat
 */
class PageEditForm extends BaseControl {

    /**
     * @var PageModel
     */
    private $pageModel;

    /**
     * @var array 
     */
    private $defaults;

    public function __construct(PageModel $pageModel) {
        $this->pageModel = $pageModel;
    }

    public function render() {
        $this->template->render(__DIR__ . "/templates/pageEdit.latte");
    }

    public function setDefaults($values) {
        $this->defaults = $values;
    }

    public function createComponentEditForm() {
        $form = new Form;

        $form->addHidden('id');

        $form->addText('title', 'Titulek: ')
                ->setRequired('Tohle pole je vyžadováno');
        $form->addText('alias', 'Alias: ')   //later added help when onMousePoint
                ->setRequired('Tohle pole je vyžadováno');
        $form->addTextArea('content');

        $submit = $form->addSubmit('save', 'Uložit změny');

        
        $submit->setAttribute('class','btn btn-success');
        
        $form->onSuccess[] = $this->formSuccess;

        // Add external success actions
        foreach ($this->onSuccess AS $action) {
            $form->onSuccess[] = $action;
        }
        if (!empty($this->defaults)) {
            $form->setDefaults($this->defaults);
        }
        return $form;
    }

    public function formSuccess($form, $values) {
        $this->pageModel->updateValues($values);
    }

}

interface IPageEditForm {

    /**
     * @return PageEditForm
     */
    public function create();
}
