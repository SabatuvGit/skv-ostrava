<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use App\Components\PhotogalleryEditForm\IPhotogalleryEditForm;
use App\Components\RemoveConfirmForm\IRemoveConfirmForm;
use App\Components\UnusedImagesForm\IUnusedImagesForm;
use App\Components\WellcomeImg\IWellcomeImg;
use App\Model\ImagesModel;
use App\Model\PhotogalleryModel;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Control;
use Nette\Forms\Form;
use Nette\Utils\ArrayHash;

/**
 * Description of PhotogalleryPresenter
 *
 * @author sabat
 */
class PhotogalleryPresenter extends BasePresenter {

    /**
     * @inject
     * @var PhotogalleryModel
     */
    public $photogalleryModel;

    /**
     * @inject
     * @var ImagesModel
     */
    public $imagesModel;


    public function actionDefault($page) {
        $photoArray = $this->photogalleryModel->getWithWelcomeImg();

        if (!$photoArray) {
            throw new BadRequestException('Žádná fotogalerie nebyla zatím vytvořena.');
        }
        $this->template->galleries = $photoArray;

        $galleriesNoLogo = $this->photogalleryModel->getPhotogalleriesNoLogo();
        if ($galleriesNoLogo) {
            $this->template->galleriesNoLogo = $galleriesNoLogo;
        }
    }

    public function actionEdit($galleryId) {
        $gallery = $this->photogalleryModel->getGallery($galleryId);
        if (!$gallery) {
            $this->error('Fotogalerie nebyla nalezena');
        }
        $this->template->gallery = $gallery;
    }

    public function actionSelect($galleryId) {
        $image = $this->imagesModel->getImagesByGallery($galleryId);
        if (!$image) {
            $this->redirect('Photogallery:linkImg', $galleryId);
        }
        $title = $this->photogalleryModel->getGalleryTitle($galleryId);

        $this->template->title = $title;
    }

    /**
     * @param int $galleryId
     */
    public function actionLinkImg($galleryId) {

        $galler = $this->photogalleryModel->getGallery($galleryId);
        if (!$galler) {
            $this->error('Fotogalerie nebyla nalezena');
        }
        $this->template->galleryName = $galler->title;
        $this->template->galleryId = $galler->id;
    }

    public function actionRemove($galleryId) {
        $gallery = $this->photogalleryModel->getGalleryWithWelcomeImg($galleryId);
        if ($gallery) {
            $this->template->gallery = $gallery;
        } elseif (!$gallery) {
            $gallery = $this->photogalleryModel->getGallery($galleryId);
            if (!$gallery) {
                $this->error('Fotogalerie nebyla nalezena');
            }
        }

        $this->template->title = $gallery->title;
    }

    public function createComponentRemoveConfirmForm() {
        $galleryId = $this->getParameter('galleryId');
        $form = $this->context->getByType(IRemoveConfirmForm::class)->create();

        $form->addSuccess($this->removeSuccess);
        $form->setRelationEntityId($galleryId);
        return $form;
    }

    public function removeSuccess($form, $values) {
        if ($form['remove']->submittedBy) {
            $this->imagesModel->rmAllunsetPhotos($values->id);
            $this->photogalleryModel->removeGallery($values->id);
            $this->flashMessage('Fotogalerie byla úspěšně odstraněna.', 'alert alert-success');
            $this->redirect('Photogallery:default');
        } elseif ($form['cancel']->submittedBy) {
            $this->redirect('Photogallery:default');
        }
    }

    /**
     * @return UI/Form
     */
    public function createComponentTolinkedImages() {
        $form = $this->context->getByType(IUnusedImagesForm::class)->create();

        $form->setRelationEntityId(-1);

        $form->addSuccess($this->linkImageGallerySuccess);

        return $form;
    }

    /**
     * @return UI/Form
     */
    public function createComponentUnlinkedImages() {
        $form = $this->context->getByType(IUnusedImagesForm::class)->create();

        $galleryId = $this->getParameter('galleryId');
        $form->setRelationEntityId($galleryId);

        $form->addSuccess($this->unlinkImageGallerySuccess);

        return $form;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Form $form
     * @param ArrayHash $values
     */
    public function linkImageGallerySuccess(Form $form, ArrayHash $values) {
        $photoId = $form->isSubmitted()->name;
        $galleryId = $this->getParameter('galleryId');

        $this->imagesModel->setGallery($photoId, $galleryId);
        $this->flashMessage('Fotografie byl úspěšně přiřazen.');

        $this->redirect('this');
    }

    public function unlinkImageGallerySuccess(Form $form, ArrayHash $values) {
        $photoId = $form->isSubmitted()->name;

        $this->imagesModel->unsetGallery($photoId);
        $this->flashMessage('Fotografie byla úspěšně odejmuta z galerie.');

        $this->redirect('this');
    }

    /**
     * @return Control WelcomeImg
     */
    public function createComponentWelcomeImgEdit() {

        $imgEdit = $this->context->getByType(IWellcomeImg::class)->create();

        $galleryId = $this->getParameter('galleryId');
        $imgEdit->setRelationParameterName('galleryId');
        $gallery = $this->photogalleryModel->getGalleryWithWelcomeImg($galleryId);
        if ($gallery) {
            if (NULL !== $gallery->getImageObj()) {
                $image = $gallery->getImageObj();
                $imgEdit->setImageId($image);
            }
        }
        return $imgEdit;
    }

    public function createComponentPhotogalleryEditForm() {
        $galleryId = $this->getParameter('galleryId');
        $form = $this->context->getByType(IPhotogalleryEditForm::class)->create();
        $form->setGalleryId($galleryId);
        $form->setDateFormat($this->context->parameters['dateFormat']);
        
        $form->addSuccess($this->editPhotogallerySucceeded);

        return $form;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Form $form
     * @param ArrayHash $values
     */
    public function editPhotogallerySucceeded(Form $form,ArrayHash $values) {
        
        $this->flashMessage('Nová galerie byla úspěšně upravena', 'alert alert-success');

        $this->redirect('Photogallery:default');
    }

    /**
     * @param none
     */
    public function createComponentPhotogalleryForm() {
        $form = $this->context->getByType(IPhotogalleryEditForm::class)->create();
        $form->setDateFormat($this->context->parameters['dateFormat']);
        $form->addSuccess($this->createPhotogallerySucceeded);

        return $form;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Form $form
     * @param ArrayHash $values
     */
    public function createPhotogallerySucceeded($form, $values) {

        $this->flashMessage('Nová galerie byla úspěšně vytvořena', 'alert alert-success');

        $this->redirect('Photogallery:default');
    }

    public function createComponentGalleryImages() {
        $form = $this->context->getByType(IUnusedImagesForm::class)->create();

        $galleryId = $this->getParameter('galleryId');

        $form->setRelationEntityId($galleryId);

        $form->addSuccess($this->setMainImageToGallery);

        return $form;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Form $form
     * @param ArrayHash $values
     */
    public function setMainImageToGallery(Form $form, ArrayHash $values) {
        $galleryId = $this->getParameter('galleryId');
        $imageId = $form->isSubmitted()->name;

        $this->photogalleryModel->setWelcomeImage($galleryId, $imageId);

        $this->redirect('Photogallery:edit', $galleryId);
    }

}
