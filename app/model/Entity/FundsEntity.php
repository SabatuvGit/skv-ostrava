<?php

namespace App\Model\Entity;

/**
 * Description of FundsEntity
 *
 * @author sabat
 */
class FundsEntity extends BaseEntity {

    /**
     * @var ImageEntity 
     */
    private $image = NULL;

    /**
     * @var Array
     */
    protected static $TYPE = array(
        1 => 'Dotace',
        2 => 'Nadace',
        3 => 'Sponzoři',
    );

    /**
     * @return int
     */
    public function getId() {
        return $this->data->id;
    }

    /**
     * @return string
     */
    public function getFundName() {
        return $this->data->fund_name;
    }

    public function getImageId() {
        return $this->data->image_id;
    }

    /**
     * @return string
     */
    public function getLink() {
        return $this->data->link;
    }

    /**
     * @return int
     */
    public function getType() {
        return $this->data->type;
    }

    public function getOrder() {
        return $this->data->order;
    }

    public function getTypeString() {
        $title = self::$TYPE;
        return $title[$this->getType()];
    }

    /**
     * @param int
     * @return String , bool
     */
    public static function getTypeName($fundNo) {
        if (array_key_exists($fundNo, self::$TYPE)) {
            $title = self::$TYPE;
            return $title[$fundNo];
        } else
            return false;
    }

    public static function getTypeArray() {
        return self::$TYPE;
    }

    /**
     * @return ImageEntity | NULL
     */
    public function getImageObj() {
        if (isset($this->image)) {
            return $this->image;
        } else {
            return NULL;
        }
    }

    /**
     * @param ImageEntity $image
     */
    public function setImageObj(ImageEntity $image) {
        $this->image = $image;
    }
    
    /**
     * @return Array
     */
    public function toArray() {
        return (array) $this->data;
    }

}
