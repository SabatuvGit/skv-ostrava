<?php

namespace App\Model;

use Nette\Object;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\Passwords;

/**
 * Users management.
 */
class UserManager extends Object implements IAuthenticator {

    /**
     * Performs an authentication.
     * @return Identity
     * @throws AuthenticationException
     */
    public function authenticate(array $credentials) {
        list($username, $password) = $credentials;

        $users = [
            'radek' => array(
                'role' => 'superadmin',
                'name' => 'Radek Sabacký',
                'password' => '$2y$10$Fb8.AWDVRblNhwFSXh1ud.7TyRI/F0QU1wOciBdqCh5qNVHnQbiBK',
            ),
            'michal' => array(
                'role' => 'admin',
                'password' => '$2y$10$Fb8.AWDVRblNhwFSXh1ud.7TyRI/F0QU1wOciBdqCh5qNVHnQbiBK',
            )
        ];

        if (!isset($users[$username]) OR ! Passwords::verify($password, $users[$username]['password'])) {
            throw new AuthenticationException('Příhlašovací heslo nebo jméno nejsou správné.', self::INVALID_CREDENTIAL);
        }

        $user = $users[$username];
        
        unset($user['password']);
        
        return new Identity($username, $user['role'], $user);
    }

}
