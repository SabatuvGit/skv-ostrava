<?php

namespace App\Components\PhotogalleryEditForm;

use App\Components\BaseControl;
use App\Model\PhotogalleryModel;
use DateTime;
use Nette\Application\UI\Form;
use Nette\Forms\IControl;
use Nette\InvalidArgumentException;
use Nette\Utils\ArrayHash;

/**
 * Description of PhotogalleryEditForm
 *
 * @author sabat
 */
class PhotogalleryEditForm extends BaseControl {

    const OKORDER = 'App\Components\PhotogalleryEditForm\PhotogalleryEditForm::validateDates';

    /**
     * @var string
     */
    private $dateFormat;

    /**
     * @var PhotogalleryModel
     */
    private $photogalleryModel = NULL;

    /**
     * @var int
     */
    private $galleryId;

    /**
     * @param PhotogalleryModel $photogalleryModel
     */
    public function __construct(PhotogalleryModel $photogalleryModel) {
        $this->photogalleryModel = $photogalleryModel;
    }

    public function render() {
        $this->template->render(__DIR__ . "/templates/default.latte");
    }

    public function setGalleryId($galleryId) {
        $this->galleryId = $galleryId;
    }

    public function setDateFormat($dateFormat) {
        $this->dateFormat = $dateFormat;
    }

    public function createComponentForm() {
        $form = new Form;

        if (!$this->dateFormat) {
            return new InvalidArgumentException('dateFormat is not set up.');
        }

        $form->addHidden('gallery_id',$this->galleryId);
        $form->addText('title', 'Název nebo město konání: ')
                ->addRule(Form::FILLED, 'Vyplňte prosím název nové galerii.');
        $form->addText('starting_date', 'Datum zahájení: ')
                ->addRule(Form::FILLED, 'Vyplňte prosím datum zahájení akce.');

        $form->addText('ending_date', 'Datum zakončení: ')
                ->addRule(PhotogalleryEditForm::OKORDER, 'Zadejte prosím datumy ve správném pořadí.', $form['starting_date']);

        $submit = $form->addSubmit('create', 'Uložit');
        $submit->setAttribute('class', 'btn btn-success');

        $form->onSuccess[] = $this->formSuccess;

        // Add external success actions
        foreach ($this->onSuccess AS $action) {
            $form->onSuccess[] = $action;
        }
        if (!empty($this->galleryId)) {
            $defaults = $this->photogalleryModel->getGallery($this->galleryId)->toArray();
            if($defaults) {
                $form->setDefaults($defaults);
            }
        }

        return $form;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSuccess(Form $form,ArrayHash $values) {
        if (empty($values['ending_date'])) {

            $starting_date = DateTime::createFromFormat($this->dateFormat, $values['starting_date']);

            $values['starting_date'] = date_format($starting_date, "Y-m-d");
            $values['ending_date'] = $values['starting_date'];
        } else {
            $starting_date = DateTime::createFromFormat($this->dateFormat, $values['starting_date']);
            $values['starting_date'] = date_format($starting_date, "Y-m-d");

            $ending_date = DateTime::createFromFormat($this->dateFormat, $values['ending_date']);
            $values['ending_date'] = date_format($ending_date, "Y-m-d");
        }
        if ($this->galleryId) {
            $this->photogalleryModel->updateValues($values);
        } else {
            $this->photogalleryModel->createNewPhotogallery($values);
        }
    }

    /**
     * @param \App\Components\PhotogalleryEditForm\IControl $control
     * @param string $starting
     * @return boolean
     */
    public static function validateDates(IControl $control, $starting) {

        $ending_date = DateTime::createFromFormat('j.n.Y', $control->getValue());

        $starting_date = DateTime::createFromFormat('j.n.Y', $starting);
        if ($ending_date != "") {
            if ($starting_date > $ending_date) {
                $control->controlPrototype->class('error');

                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

}

interface IPhotogalleryEditForm {

    /**
     * @return PhotogalleryEditForm
     */
    public function create();
}
