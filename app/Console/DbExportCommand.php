<?php

namespace App\Console;

use App\Components\SQLBatchOperator;
use Nette\InvalidStateException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DbExportCommand extends Command {

    /**
     * @var SQLBatchOperator
     */
    private $batchOperator = NULL;

    public function injectBatchOperator(SQLBatchOperator $batchOperator) {
        if ($this->batchOperator) {
            throw new InvalidStateException('SQLBatchOperator allready been set.');
        }

        $this->batchOperator = $batchOperator;
    }

    protected function configure() {
        $this->setName('db:export')
                ->setDescription('Export my DB SQL')
                ->addArgument('filename', InputArgument::OPTIONAL, "Filename (can have sql.gz extension for apply compression)");
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $time = -microtime(TRUE);
        
        $filename = $input->getArgument('filename');
        
        $file = $this->batchOperator->export($filename);
        
        $time += microtime(TRUE);

        $output->writeln('Databse successfully exported to <comment>' . $file . '</comment> file. (<info>' . round($time, 2) . 's</info>)');
    }

}
