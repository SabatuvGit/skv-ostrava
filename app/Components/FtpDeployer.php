<?php

namespace App\Components;

use Deployment\CliRunner;
use Deployment\Deployer;
use Deployment\FtpServer;
use Deployment\Logger;
use Deployment\Server;
use Deployment\SshServer;
use Exception;
use Nette\Object;

/**
 * Deployment to FTP server
 */
class FtpDeployer extends Object {

	/** @var Logger */
	private $logger;

	/** @var string  test|generate|NULL */
	private $mode;

	/** @var array */
	private $config;

	/**
	 * @var array
	 */
	private $defaultIgnores = ['*.bak', '.svn', '.git*', 'Thumbs.db', '.DS_Store'];

	/** @var array */
	private $defaultConfig = [
		'passivemode' => TRUE,
		'ignore' => '',
		'allowdelete' => TRUE,
		'purge' => '',
		'before' => '',
		'after' => '',
		'preprocess' => TRUE,
	];

	/**
	 *
	 * @param array $config
	 * @param string $mode test|generate|NULL
	 */
	public function __construct(array $config, $mode = NULL) {
		$this->logger = new Logger('php://memory');

		$config += [
			'log' => __DIR__ . '/../../log/deployment.log',
			'tempdir' => sys_get_temp_dir() . '/deployment',
			'colors' => $this->useColors(),
		];

		$this->config = $config;
		$this->mode = $mode;
	}

	public function run() {
		$this->logger = new Logger($this->config['log']);
		$this->logger->useColors = (bool) $this->config['colors'];

		if (!is_dir($tempDir = $this->config['tempdir'])) {
			$this->logger->log("Creating temporary directory $tempDir");
			mkdir($tempDir);
		}

		$this->measurement($tempDir);
	}

	/**
	 * @param string $tempDir
	 */
	private function measurement($tempDir) {
		$timeStart = time();
		$this->logger->log("Started at " . date('[Y/m/d H:i]'));

		if (isset($this->config['remote']) AND is_string($this->config['remote'])) {
			$this->config = ['' => $this->config];
		}

		$this->deployAll($tempDir);

		$time = time() - $timeStart;
		$this->logger->log("\nFinished at " . date('[Y/m/d H:i]') . " (in $time seconds)", 'lime');
	}

	/**
	 * @return bool
	 */
	private function useColors() {
		if (PHP_SAPI === 'cli') {
			$posix = function_exists('posix_isatty') AND posix_isatty(STDOUT);

			if ($posix OR getenv('ConEmuANSI') === 'ON' OR getenv('ANSICON') !== FALSE) {
				return True;
			}
		}

		return False;
	}

	/**
	 * @param string $tempDir
	 */
	private function deployAll($tempDir) {
		foreach ($this->config as $section => $cfg) {
			if (!is_array($cfg)) {
				continue;
			}

			$this->logger->log("\nDeploying $section");

			$deployment = $this->createDeployer($cfg, $tempDir);

			if ($this->mode === 'generate') {
				$this->logger->log('Scanning files');
				$localFiles = $deployment->collectFiles();
				$this->logger->log("Saved " . $deployment->writeDeploymentFile($localFiles));
				continue;
			}

			$this->logConfig($deployment);

			$deployment->deploy();
		}
	}

	/**
	 * @param Deployer $deployment
	 */
	private function logConfig(Deployer $deployment) {
		if ($deployment->testMode) {
			$this->logger->log('Test mode');
		}

		if (!$deployment->allowDelete) {
			$this->logger->log('Deleting disabled');
		}
	}

	/**
	 * @var array $configExternal
	 * @param $tempDir
	 * @return Deployer
	 * @throws Exception
	 */
	private function createDeployer(array $configExternal, $tempDir) {
		$config = array_change_key_case($configExternal, CASE_LOWER) + $this->defaultConfig;

		if (empty($config['remote']) OR !parse_url($config['remote'])) {
			throw new Exception("Missing or invalid 'remote' URL in config.");
		}

		$server = $this->serverConnection($config);

		$deployment = new Deployer($server, $config['local'], $this->logger);

		$deployment->ignoreMasks = array_merge($this->defaultIgnores, CliRunner::toArray($config['ignore']));

		return $this->configureDeployer($deployment, $tempDir);
	}


	/**
	 * @param Deployer $deployment
	 * @param $tempDir
	 * @return Deployer
	 */
	private function configureDeployer(Deployer $deployment, $tempDir) {
		$deployment->deploymentFile = empty($config['deploymentfile']) ? $deployment->deploymentFile : $config['deploymentfile'];
		$deployment->allowDelete = $config['allowdelete'];
		$deployment->toPurge = CliRunner::toArray($config['purge'], TRUE);
		$deployment->runBefore = CliRunner::toArray($config['before'], TRUE);
		$deployment->runAfter = CliRunner::toArray($config['after'], TRUE);
		$deployment->testMode = !empty($config['test']) || $this->mode === 'test';
		$deployment->tempDir = $tempDir;

		return $deployment;
	}

	/**
	 * @param array $config
	 * @return Server
	 */
	private function serverConnection(array $config) {
		if (parse_url($config['remote'], PHP_URL_SCHEME) === 'sftp') {
			return new SshServer($config['remote']);
		}

		return new FtpServer($config['remote'], (bool) $config['passivemode']);
	}

}
