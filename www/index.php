<?php

use Nette\Application\Application;

// Uncomment this line if you must temporarily take down your site for maintenance.
// require __DIR__ . '/.maintenance.php';

/*if(php_sapi_name() == "cli") {
    echo "test";
    exit;
}*/

$container = require __DIR__ . '/../app/bootstrap.php';

$container->getByType(Application::class)->run();
