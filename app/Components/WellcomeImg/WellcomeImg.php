<?php

namespace App\Components\WellcomeImg;

use App\Components\BaseControl;
use App\Model\Entity\ImageEntity;

/**
 * Description of WelcomeImg
 *
 * @author sabat
 */
class WellcomeImg extends BaseControl {

    /**
     * @var string
     */
    private $relationParameterName;

    /**
     * @var ImageEntity 
     */
    private $imageObj;

    public function render() {
        $template = $this->template;
        $template->setFile(__DIR__ . "/template/default.latte");

        $paramId = $this->getPresenter()->getParameter($this->relationParameterName);
        if (isset($this->imageObj)) {
            $template->selectLink = $this->presenter->link(":" . $this->getPresenter()->getName() . ':select', $paramId);
            $template->image = $this->imageObj;
            $template->render();
        } else {
            $template->selectLink = $this->presenter->link(":" . $this->getPresenter()->getName() . ':select', $paramId);
            $template->render();
        }
    }

    public function setRelationParameterName($parameterName) {
        $this->relationParameterName = $parameterName;
    }

    public function setImageId($imageObj) {
        $this->imageObj = $imageObj;
    }

}

interface IWellcomeImg {

    /**
     * @return WellcomeImg
     */
    function create();
}
