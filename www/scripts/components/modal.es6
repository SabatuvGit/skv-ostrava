let $ = require('jquery');

let modal = function(selector) {
    let element = $(selector);
    let modalWindow = $('<div class="window"/>');
    
    element.on('click',function(event){
       event.preventDefault();
       let url = $(this).attr('href'); 
       console.log(url);
       
       $.ajax(url).done(function(data){
           console.log(data);
           
           modalWindow.html(data);
           
           $('body').append(modalWindow);
       });
    });
    
};

module.exports = modal;
