<?php

namespace App\Model\Entity;

/**
 * Description of BaseEntity
 *
 * @author sabat
 */
class PageEntity extends BaseEntity {

   
    /**
     * @return int
     */
    public function getId() {
        return $this->data->id;
    }

    /**
     * @return string
     */
    public function getAlias() {
        return $this->data->alias;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->data->title;
    }

    /**
     * @return string
     */
    public function getContent() {
        return $this->data->content;
    }

    /**
     * @return string
     */
    public function getContentPreview() {
        return substr($this->data->content, 0, 100);
    }

    public function toArray() {
        return (array)$this->data;
    }

}
