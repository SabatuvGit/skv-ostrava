<?php

namespace App\Model;

use App\Model\Entity\PhotogalleryEntity;
use Nette\Database\Context;

/**
 * Description of PhotogalleryModel
 *
 * @author sabat
 */
class PhotogalleryModel extends BaseModel {

    /**
     * @var String
     */
    const TABLE_NAME = 'photogallery';

    /**
     * @var string
     */
    private $dateFormat;

    /**
     * @param string $dateFormat
     * @param Context $connection
     */
    public function __construct($dateFormat, Context $connection) {
        parent::__construct($connection);

        $this->dateFormat = $dateFormat;
    }

    /**
     * 
     * @param int $galleryId
     * @return string,bool
     */
    public function getGalleryTitle($galleryId) {
        $sql = "SELECT title FROM photogallery "
                . "WHERE gallery_id = ? ";
        $title = $this->connection->queryArgs($sql, array($galleryId))
                ->fetch();
        if ($title) {
            return $title->title;
        }
        return False;
    }

    /**
     * 
     * @param int $galleryId
     * @return PhotogalleryEntity,bool
     */
    public function getGallery($galleryId) {
        $sql = "SELECT * FROM photogallery "
                . "WHERE gallery_id = ? ";
        $gallery = $this->connection->queryArgs($sql, array($galleryId))
                ->fetch();

        if ($gallery) {
            return new PhotogalleryEntity($gallery, $this->dateFormat);
        }
        return False;
    }

    public function setWelcomeImage($galleryId, $imageId) {
        $photo = $this->connection->table(self::TABLE_NAME)->where('gallery_id', $galleryId);
        $photo->update(array(
            'welcome_image_id' => $imageId,
        ));
    }

    /**
     * @param void 
     * @return []PhotogalleryEntity,bool
     */
    public function getPhotogalleries() {
        $sql = "SELECT * FROM photogallery "
                . "ORDER BY ending_date DESC";
        $galleries = $this->connection->query($sql)->fetchAll();
        $photosArray = array();

        if ($galleries) {
            foreach ($galleries as $row) {
                $photosArray[] = new PhotogalleryEntity($row, $this->dateFormat);
            }
            return $photosArray;
        }
        return false;
    }

    /**
     * @param array
     * @return bool
     */
    public function createNewPhotogallery($values) {
        $this->connection->table(self::TABLE_NAME)->insert($values);

        return true;
    }

    /**
     * 
     * @param array $values
     */
    public function updateValues($values) {
        $page = $this->connection->table(self::TABLE_NAME)->where('gallery_id', $values->gallery_id);
        $page->update($values);
    }

    /**
     * 
     * @return PhotogalleryEntity array
     */
    public function getWithWelcomeImg() {
        $sql = "SELECT * "
                . "FROM photogallery,image "
                . "WHERE image.id=photogallery.welcome_image_id "
                . "ORDER BY ending_date DESC";
        $galleries = $this->connection->query($sql)->fetchAll();
        $photosArray = array();
        if (!empty($galleries)) {
            foreach ($galleries as $gallery) {
                $image = new Entity\ImageEntity($gallery);
                $newGallery = new PhotogalleryEntity($gallery, $this->dateFormat);
                $newGallery->setImageObj($image);
                $photosArray[] = $newGallery;
            }
            return $photosArray;
        } else
            return false;
    }

    /**
     * 
     * @param PhotogalleryEntity $galleryEntity
     */
    public function getGalleryWithWelcomeImg($galleryID) {
        $sql = "SELECT * "
                . "FROM photogallery,image "
                . "WHERE image.id=photogallery.welcome_image_id "
                . "AND photogallery.gallery_id = ?";
        $gallery = $this->connection->queryArgs($sql, array($galleryID))->fetch();
        if ($gallery) {
            $image = new Entity\ImageEntity($gallery);
            $newGallery = new PhotogalleryEntity($gallery, $this->dateFormat);
            $newGallery->setImageObj($image);
            return $newGallery;
        } else
            return false;
    }

    public function getPhotogalleriesNoLogo() {
        $sql = "SELECT * "
                . "FROM photogallery "
                . "WHERE photogallery.welcome_image_id is NULL "
                . "ORDER BY ending_date DESC";
        $galleries = $this->connection->query($sql)->fetchAll();
        $photosArray = array();
        if (!empty($galleries)) {
            foreach ($galleries as $gallery) {
                $photosArray[] = new PhotogalleryEntity($gallery, $this->dateFormat);
            }
            return $photosArray;
        } else {
            return false;
        }
    }
    
    public function removeGallery($galleryId) {
        $this->setWelcomeImage($galleryId, -1);
        $this->connection->table('photogallery')->get($galleryId)->delete();
    }

}
