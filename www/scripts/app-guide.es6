let $ = require('jquery');
let mainMenu = require('./components/menu-main.es6');


$(function() {
    mainMenu('.js-menu-item', '.js-menu-link', '.js-menu-sub');
});
