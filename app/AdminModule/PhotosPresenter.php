<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use App\Forms\PhotosFormFactory;
use App\Model\ImagesModel;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

/**
 * Description of PhotosPresenter
 *
 * @author sabat
 */
class PhotosPresenter extends BasePresenter {

    /**
     * @inject
     * @var PhotosFormFactory
     */
    public $photosFormFactory;

    /**
     * @inject
     * @var ImagesModel
     */
    public $photosModel;


    public function renderDefault() {
        $this->photosModel->getImageSizes();

        $photos = $this->photosModel->getImagesByGallery(-1);
        
        if ($photos) {
            $this->template->photos = $photos;
        }
    }

    /**
     * @return UI/Form
     */
    public function createComponentUploadImageForm() {
        $form = $this->photosFormFactory->uploadImage($this->photosModel);
        $form->onSuccess[] = array($this, 'uploadImageSucceeded');
        return $form;
    }
    
    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Form $form
     * @param ArrayHash $values
     */
    public function uploadImageSucceeded($form, $values) {

        $this->flashMessage('Obrázek byl úspěšně nahrán', 'alert alert-success');
        $this->redirect('this');
    }

}
