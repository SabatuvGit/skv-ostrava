<?php

define('APP_DIR', __DIR__);
define('WWW_DIR', __DIR__ . '/../www');
define('TEMP_DIR', isset($_SERVER['TEMP_DIR']) ? $_SERVER['TEMP_DIR'] : __DIR__ . '/../temp');
define('LOG_DIR',  isset($_SERVER['LOG_DIR']) ? $_SERVER['LOG_DIR'] : __DIR__ . '/../log');

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

//$configurator->setDebugMode(FALSE); // enable for your remote IP
$configurator->enableDebugger(__DIR__ . '/../log', 'sabradat@gmail.com');

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
