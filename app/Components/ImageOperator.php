<?php

namespace App\Components;

use App\Model\Entity\ImageEntity;
use App\Model\ImagesModel;
use Nette\Object;
use Nette\Utils\Image;

/**
 * Description of ImageOperator
 *
 * @author sabat
 */
class ImageOperator extends Object {

    /**
     * @var string
     */
    private $dir = '/var/www/skvostrava/www/data/';

    /**
     * @var ImagesModel 
     */
    private $photosModel;

    /**
     * @param ImagesModel $photosModel
     */
    public function __construct(ImagesModel $photosModel) {

        $this->photosModel = $photosModel;
    }

    /**
     * @return string array
     */
    public function generateImages() {
        $photos = $this->photosModel->getAllImages();
        $regen = array();
        foreach ($photos as $photo) {

            $image = Image::fromFile('/var/www/skvostrava/www/data/' . $photo->getFullName());
            $list = $this->generateMiniatures($image, $photo->getId());
            if (!empty($list)) {
                $regen[$photo->getName()] = $list;
            }
        }
        return $regen;
    }

    /**
     * @param Image $image
     * @param int $photoId
     * @return array string
     */
    public function generateMiniatures($image, $photoId) {
        $photo = $this->photosModel->getImage($photoId);
        $sizes = $this->photosModel->getImageSizes();
        $regenerated = array();
        if ($photo) {
            foreach ($sizes as $size) {
                $width = $image->width;
                $height = $image->height;

                $imgName = $this->generateName($photo,$size[0],$size[1]);
                if ($width > $height) {
                    $regenerated = $this->generate($image, $size[0], NULL,$imgName);
                } else {
                    $regenerated = $this->generate($image, NULL, $size[1],$imgName);
                }
            }
        }
        return $regenerated;
    }

    /**
     * @param Image $image
     * @param int $width
     * @param int $height
     */
    public function generate($image, $width, $height, $imgName) {
        $regenerated = array();
       if (!file_exists($this->dir . $imgName)) {
            $image->resize($width, $height);
            $image->sharpen();
            $image->save($this->dir . $imgName);

            $regenerated[] = $imgName;
        }
        return $regenerated;
    }

    /**
     * @param ImageEntity $photo
     * @param int $width
     * @param int $height
     * @return array string
     */
    public function generateName($photo, $width, $height) {

        if ($width == NULL) {
            $imgName = $photo->getName() . "_" . "auto" . "_" . $height . "_." . $photo->getFormat();
        } elseif ($height == NULL) {
            $imgName = $photo->getName() . "_" . $width . "_" . "auto" . "_." . $photo->getFormat();
        } else {
            $imgName = $photo->getName() . "_" . $width . "_" . $height . "_." . $photo->getFormat();
        }

        return $imgName;
    }

}
