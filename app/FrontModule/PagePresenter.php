<?php

namespace App\FrontModule\Presenters;

use App\Model\PageModel;
use Nette\Application\BadRequestException;

/*function dd($data) {
    \Tracy\Debugger::barDump($data);
}*/

class PagePresenter extends BasePresenter {

    /**
     * @inject
     * @var PageModel
     */
    public $pageModel;
    

    public function actionDefault($pageAlias) {
        
        
        $page = $this->pageModel->getPageByAlias($pageAlias);
        
        if(!$page) {
            throw new BadRequestException('Page with alias [' . $pageAlias . '] not found');
        }
        $this->template->page = $page;
    }

}
