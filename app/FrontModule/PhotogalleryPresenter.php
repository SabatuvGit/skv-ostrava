<?php

namespace App\FrontModule\Presenters;

use App\Model\ImagesModel;
use App\Model\PhotogalleryModel;
use Nette\Application\BadRequestException;

class PhotogalleryPresenter extends BasePresenter {

    /**
     * @inject
     * @var PhotogalleryModel
     */
    public $photogalleryModel;

    /**
     * @inject
     * @var ImagesModel
     */
    public $imagesModel;

    public function actionDefault($page) {
        $photoArray = $this->photogalleryModel->getWithWelcomeImg();

        if (!$photoArray) {
            throw new BadRequestException('Zatím nejsou k dispozici žádné fotogalerie.');
        }

        $this->template->photos = $photoArray;
    }

    public function actionShowphotos($galleryId) {
        $title = $this->photogalleryModel->getGalleryTitle($galleryId);

        if (!$title) {
            throw new BadRequestException('Odpovídající galerie nebyla nalezena.');
        }
        $this->template->title = $title;

        $photos = $this->imagesModel->getImagesByGallery($galleryId);

        if ($photos) {
            $this->template->photos = $photos;
        }
    }

}
