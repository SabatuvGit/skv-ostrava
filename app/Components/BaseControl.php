<?php

namespace App\Components;

use Nette\Application\UI\Control;

/**
 * Description of BaseComponent
 *
 * @author sabat
 */
abstract class BaseControl extends Control {

    protected $onSuccess = [];

    public function addSuccess(callable $action) {
        $this->onSuccess[] = $action;
    }

}
