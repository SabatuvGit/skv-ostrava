<?php

namespace App\FrontModule\Presenters;

use Nette;
use App\Forms\SignFormFactory;

class SignPresenter extends BasePresenter {

    /**
     * @persistent
     */
    public $backlink;


    /**
     * @inject
     * @var SignFormFactory
     */
    public $signFormFactory;

    /**
     * Sign-in form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm() {
        $form = $this->signFormFactory->create();
        $form->onSuccess[] = array($this, 'signInSucceeded');
        
        return $form;
    }
    
    public function signInSucceeded() {        
        if ($this->backlink) {
            $this->restoreRequest($this->backlink);
        }
        
        $this->redirect('Homepage:');
        
//        $element = \Nette\Utils\Html::el('a');
//        $element->addAttributes('href', $this->link('Homepage:'));
//        $element->setText('Kontakt');
//        $element->add($child);
//        $element->add('gdfgdf');
//        $element->getHtml();
    }

    public function actionOut() {
        $this->getUser()->logout();
        $this->flashMessage('Byl jste úspěšně odhlášen.');
        $this->redirect('in');
    }

}
