let $ = require('jquery');

/**
 * @param {string} selectorItem
 * @param {string} selectorLink
 * @param {string} selectorSub
 */
let menu = function(selectorItem, selectorLink, selectorSub) {
    let item = $(selectorItem);

    $(selectorSub).hide();
    $(selectorLink).addClass('menu__link--arrow');

    item.find(selectorLink).on('click', function(event) {
        event.preventDefault();

        let link = $(this);

        link.toggleClass('menu__link--active');
        link.parent().find(selectorSub).toggle();
    });

};

module.exports = menu;
