<?php

namespace App\Console;

use App\Components\ImageOperator;
use PDepend\Source\Parser\InvalidStateException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of ThumbnailsGeneratorCommand
 *
 * @author sabat
 */
class ThumbnailsGeneratorCommand extends Command {

    /**
     * @var string 
     */
    private $dir = '/data';

    /**
     * @var ImageOperator
     */
    private $imageOperator = NULL;

    public function injectImageOperator(ImageOperator $imageOperator) {
        if ($this->imageOperator) {
            throw new InvalidStateException('ImageOperator allready has been set.');
        }

        $this->imageOperator = $imageOperator;
    }

    public function configure() {
        $this->setName('image:generator')
                ->setDescription('Generate thumbnails for all pictures in project');
    }

    public function execute(InputInterface $input, OutputInterface $output) {

        $time = -microtime(TRUE);

        $results = $this->imageOperator->generateImages();
        foreach ($results as $result => $pictures) {
            $output->writeln('For picture ' . $result . ' : ');
            foreach ($pictures as $picture) {
                $output->writeln($picture . ' was generated.');
            }
        }
        $time += microtime(TRUE);
    }

}
