<?php

namespace App\Model;

use App\Model\Entity\ImageEntity;

/**
 * Description of PhotosModel
 *
 * @author sabat
 */
class ImagesModel extends BaseModel {

    /**
     * @var String
     */
    const TABLE_NAME = 'image';

    /**
     * [Width, Height]
     * @var array
     */
    const IMAGE_BIG = [640, 480];
    const IMAGE_PREVIEW = [300, 225];
    const IMAGE_BANNER = [250, 187];
    const IMAGE_ICON = [150, 112];
    const IMAGE_THUMBNAIL = [80, 60];

    /**
     * 
     * @param array $values
     * @return int
     */
    public function createNew($values) {
        $newId = $this->connection->table(self::TABLE_NAME)->insert($values);
        return $newId;
    }

    /**
     * @return array
     */
    public function getAllImages() {
        $sql = "SELECT * FROM image ";
        $photos = $this->connection->query($sql);
        $photosArray = array();
        foreach ($photos as $row) {
            $photosArray[] = new ImageEntity($row);
        }
        return $photosArray;
    }

    /**
     * @return array
     */
    public function getImageSizes() {
        $reflection = new \ReflectionClass($this);
        $constants = $reflection->getConstants();

        $results = [];

        foreach ($constants AS $name => $constant) {
            if (strpos($name, 'IMAGE_') === False) {
                continue;
            }

            $results[$name] = $constant;
        }

        return $results;
    }

    /**
     * @param string $galleryId
     * @return bool|PhotoEntityArray
     */
    public function getImagesByGallery($galleryId) {
        $sql = "SELECT * FROM image "
                . "WHERE photogallery_id = ?";
        $photos = $this->connection->queryArgs($sql, array($galleryId))->fetchAll();

        $fundsArray = array();
        foreach ($photos as $row) {
            $fundsArray[] = new ImageEntity($row);
        }
        if (!empty($fundsArray)) {
            return $fundsArray;
        }
        return false;
    }

    /**
     * @param string $galleryId
     * @return bool|PhotoEntityArray
     */
    public function getImage($photoId) {
        $sql = "SELECT * FROM image "
                . "WHERE id = ?";
        $photo = $this->connection->queryArgs($sql, array($photoId))->fetch();
        if ($photo) {
            return new ImageEntity($photo);
        }
        return false;
    }

    /**
     * @param string $galleryId
     * @return bool|PhotoEntityArray
     */
    public function getUnsetImages() {
        return $this->getImagesByGallery(-1);
    }

    public function setGallery($imageId, $galleryId) {
        $photo = $this->connection->table(self::TABLE_NAME)->get($imageId);
        $photo->update(array(
            'photogallery_id' => new \Nette\Database\SqlLiteral($galleryId)
        ));
    }

    public function unsetGallery($imageId) {
        $photo = $this->connection->table(self::TABLE_NAME)->get($imageId);
        $photo->update(array(
            'photogallery_id' => -1,
        ));
    }

    public function addAllunsetPhotos($galleryId) {
        $photos = $this->getImagesByGallery(-1);
        foreach ($photos as $photo) {
            $this->setGallery($photo->getId(), $galleryId);
        }
    }

    public function rmAllunsetPhotos($galleryId) {
        $photos = $this->getImagesByGallery($galleryId);
        if ($photos) {
            foreach ($photos as $photo) {
                $this->setGallery($photo->getId(), -1);
            }
        }
    }

    public function saveImage($values) {
        $this->connection->table(self::TABLE_NAME)->insert($values);
    }

    public function deleteImage($values) {
        $this->connection->table(self::TABLE_NAME)->where('id', $values->id)->delete();
    }

}
