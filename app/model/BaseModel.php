<?php

namespace App\Model;

use Nette\Database\Context;
use Nette\Object;

/**
 * Description of BaseModel
 *
 * @author sabat
 */
abstract class BaseModel extends Object {

    /**
     * @var Context
     */
    protected $connection;

    /**
     * @param Context $connection
     */
    public function __construct(Context $connection) {
        $this->connection = $connection;
    }

    /**
     * @return Context
     */
    public function getConnection() {
        return $this->connection;
    }

}
