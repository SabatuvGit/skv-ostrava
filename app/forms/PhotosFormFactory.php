<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Forms;

use App\Components\ImageOperator;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Utils\Image;

/**
 * Description of PhotosFormFactory
 *
 * @author sabat
 */
class PhotosFormFactory extends Object {

    /**
     *
     * @var String
     */
    private $dir;

    /**
     *
     * @var ImageModel
     */
    private $photosModel;

    /**
     * 
     * @param String $dir
     */
    public function __construct($dir) {
        $this->dir = $dir;
    }

    public function uploadImage($pageModel) {
        $this->photosModel = $pageModel;

        $form = new Form;

        $form->addUpload('image', 'Vyberte obrázek:', 'TRUE')
                ->addRule(Form::IMAGE, 'Soubor musí být JPEG, PNG nebo GIF.');
        
        $button = $form->addSubmit('save', 'Nahrát');

        $button->setAttribute('class', 'btn btn-success pull-right');

        $form->onSuccess[] = array($this, 'uploadImageSucceeded');

        return $form;
    }

    public function uploadImageSucceeded($form, $values) {

        foreach ($values['image'] as $file) {

            if ($file->isOk() and $file->isImage()) {

                $imageName = $file->getSanitizedName();
                $fileName = explode(".", $imageName);  //split string into array
                $newName = tempnam($this->dir, $fileName[0] . time());  //generate unique name of file
                unlink($newName);  //delete generated unique file
                $file->move($newName . "." . $fileName[count($fileName) - 1]);

                $name = explode("/", $newName);  //split string into array

                $newPhotoId = $this->photosModel->createNew(array(
                    'name' => $name[count($name) - 1],
                    'format' => $fileName[count($fileName) - 1],
                ));

                //generate miniatures
                $imageOperator = new ImageOperator($this->photosModel);

                $image = Image::fromFile($file->getTemporaryFile());
                $imageOperator->generateMiniatures($image, $newPhotoId);
            }
        }
    }

}
